import {logger, authorizeRoute } from "./authorization.js";

import fs from "fs";
import cors from "cors";
import express from "express";

const app = express();

const IP_ADDRESS = process.env.OPENSHIFT_NODEJS_IP || "0.0.0.0";
const PORT = ( process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT ) || 3500;

const corsOptions = {
	origin: '*', //['http://localhost:3000', 'https://localhost:3000', 'http://127.0.0.1:5555', 'https://127.0.0.1:5555.', 'http://localhost:4545', 'https://localhost:4545'],
	allowedHeaders: ["Content-Type", "Authorization", "Access-Control-Allow-Methods", "Access-Control-Request-Headers", "Access-Control-Allow-Origin"],
	credentials: true,
	enablePreflight: true
}
const jobsData = JSON.parse(fs.readFileSync('jobsData.json', 'utf8'));

app.use(logger);

app.use(function (req, res, next) {

	const allowedOrigins = ['http://127.0.0.1:3000', 'http://localhost:3000',
		'http://127.0.0.1:8000', 'http://localhost:8000',
		'http://127.0.0.1:8081', 'http://localhost:8081',
		'http://127.0.0.1:4545', 'http://localhost:4545',
		'http://192.168.1.63:4545', 'http://192.168.1.63:4545',
		'http://192.168.1.63:8000', 'http://192.168.1.63:8000',
		'http://192.168.1.63:3000', 'http://192.168.1.63:3000'];

	const origin = req.headers.origin;
	if (allowedOrigins.includes(origin)) {
		res.header('Access-Control-Allow-Origin', origin);
	}

	// console.log(JSON.stringify(req.headers));

	// Website you wish to allow to connect
	//res.header('Access-Control-Allow-Origin', 'http://localhost:3000');

	// Request methods you wish to allow
	res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

	// Request headers you wish to allow
	res.header('Access-Control-Allow-Headers', 'x-access-token, Origin, X-Requested-With, Content-Type, Accept');

	// Set to true if you need the website to include cookies in the requests sent
	// to the API (e.g. in case you use sessions)
	res.header('Access-Control-Allow-Credentials', true);

	// Pass to next layer of middleware
	next();
});


app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(express.static('public'));

app.use(cors(corsOptions));
app.options('*', cors(corsOptions))


app.get('/', function(req, res){
	console.log("Received request");
	res.send("Hello World!");
});

app.get("/jobs", function (req, res) {
//  res.send("<h1>I am a header.</h1>");
	res.json(jobsData);
});


// TODO refactor routes
// https://www.coreycleary.me/avoid-manually-prepending-api-to-every-express-route-with-this-simple-method


app.use(authorizeRoute);

app.get("/api/test/admin", (req, res) => {
	console.log("Admin board - handler");
	res.send("Admin Board!");
});

app.listen(PORT, function() {
	console.log("Server started on port: " + IP_ADDRESS + ":" +  PORT);				
});
