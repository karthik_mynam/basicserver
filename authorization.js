
import { CognitoJwtVerifier } from "aws-jwt-verify";

// Verifier that expects valid access tokens:
const verifier = CognitoJwtVerifier.create({
    userPoolId: "us-east-1_DuvcCD4zm",
    tokenUse: "access",
    clientId: "63dqsptnrqkdpd4osgqvn42akf",
});

const validateCognitoToken = async (token) => {
    // Verifies the access JWT token
    try {
        const payload = await verifier.verify(token);
        // await new Promise(resolve => setTimeout(resolve, 5000));
       //  console.log("Token is valid. Payload: " + JSON.stringify(payload));

        return true;
    } catch (ex) {
        console.log("Token not valid: " + ex);
    }

    return false;
}


const authorizeRoute = (req, res, next) => {
    console.log('private route hit!');
    const accessJWTToken = req.headers.authorization;
   // console.log("Token: " + accessJWTToken);

    if (accessJWTToken) {
        validateCognitoToken(accessJWTToken).then((isValid) => {
            console.log("token is: " + isValid);

            if (isValid) {
                next();
            } else {
                return res.status(401).send('Unauthorized request');
            }

        }).catch((ex) => {
            console.log("Cannot verify token");
            return res.status(401).send('Unauthorized request');
        })
    }
};

const logger = (req, res, next) => {
    console.log('Request: ' + new Date().toString() + ' ' + req.method + " " + req.originalUrl);
    next();
}


export {
    logger,
    authorizeRoute
};